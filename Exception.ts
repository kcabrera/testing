export class MunicipalityException extends Error { 
    constructor(message: string) {
        super(`MunicipalityException: ${ message }`);
    }
}
export class LocationException extends Error {
    constructor(message: string) {
        super(`LocationException: ${ message }`);
    }
}

export class ZipCodeException extends Error {
    constructor(message: string) {
        super(`ZipCodeException: ${ message }`);
    }
}
import ValueObject from "./ValueObject";
import { isString, minLength, maxLength, exactLength, containNumbers } from "./validators";
import { MunicipalityException, LocationException, ZipCodeException } from "./Exception";

export default class Address extends ValueObject {

    constructor(
        private _municipality: string,
        private _location: string,
        private _zipCode: string
    ) { 
        super();

        super.validate(
            isString(MunicipalityException),
            minLength(3)(MunicipalityException),
            maxLength(12)(MunicipalityException)
        )(_municipality);

        super.validate(
            isString(LocationException),
            minLength(3)(LocationException),
        )(_location);

        super.validate(
            isString(ZipCodeException),
            exactLength(5)(ZipCodeException),   
        )(_zipCode); 
        
    }
}
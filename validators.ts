export function isString(Exception: any) {
    return function (value: string) {
        if (typeof value !== 'string') {
            throw new Exception(`Value "${ value }" must be a String`);
        }
    }
}

export function minLength(min: number) {
    return function (Exception: any) {
        return function (value: string) {
            if (value.length < min) {
                throw new Exception(`Value "${ value }" must have more than ${ min } characters`);
            }
        }
    }
}

export function maxLength(max: number) {
    return function (Exception: any) {
        return function (value: string) { 
            if (value.length > max) {
                throw new Exception(`Value "${ value }" have less then ${ max } characters`);
            }
        }
    }
}

export function containNumbers(e: any) { 
    return function (Exception: any) {
        return function (value: string) {
            if (!parseInt(value)) {
                throw new Exception(`Value "${ value }" contains only numbers`);
            }
        }
    }
} 

export function exactLength(length: number) {
    return function (Exception: any) {
        return function (value: string) {
            if (value.length !== length) {
                throw new Exception(`Value "${ value }" should have length ${ length }`);
            }
        }
    }
}
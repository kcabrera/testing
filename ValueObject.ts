type ValidatorFn<T> = (value: T) => void;

export default abstract class ValueObject {
    protected validate<T>(...validatorFns: ValidatorFn<T>[]) {
        return function(value: T) { 
            validatorFns.forEach(validator => validator(value));
        }
    };
}